# LIS4381 - Mobile Web Application Development

## Glennetria Harold

### Assignment 2 Requirements:

*Three Parts:*

1. Healthy Recipes Mobile App
2. Chapter Questions
3. Bitbucket repo link 


#### README.md file should include the following items:

* Screenshot of running Android Studio first and second user interface
* Screenshot of all 3 Java skill sets running
* Bitbucket repo link for this assignment

#### Assignment Screenshots:
*Screenshot of Android Studio - Healthy Recipes*

| Activity_Main: | Activity_Recipe: |
| ------- | ------- |
| ![First User Interface Screenshot](img/Screenshot_1600714646.png)|![Second User Interface Screenshot](img/Screenshot_1600714664.png) |

*Screenshots of Java Skill Sets*

|Skill Set 1: Even Or Odd | Skill Set 2 LargestNumber | Skill Set 3: Arrays and Loops |
| ------- | ------- | ------ |
| ![Skill Set 1 Screenshot](img/SkillSet1.PNG) | ![Skill Set 2 Screenshot](img/SkillSet2.PNG)| ![Skill Set 3 Screenshot](img/SkillSet3.PNG) |


#### Tutorial Links:
*Assignment 2:*
[A2 Repository Link](https://bitbucket.org/gjh16/lis4381 "gjh16 LIS4381 Main Repo")