<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="This is my online protfolio created for LIS4381 with various mobile web app developments.">
		<meta name="author" content="Glennetria J. Harold">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body style="background-image: url('../img/bg.jpg'); background-repeat: no-repeat; background-size: cover;">

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Android Studio Healthy Recipie app containing first and second user interface
				</p>
				<table style="width:100%;">
					<tr>
						<th style="border-right: 5px solid white;">User Interface 1</th>
						<th>User Interface 2</th>
					</tr>
					<tr>
						<td style="border-right: 5px solid white;"><img src="img/Screenshot_1600714646.png" class="img-responsive center-block" alt="JDK Installation"></td>
						<td><img src="img/Screenshot_1600714664.png" class="img-responsive center-block" alt="Android Studio Installation"></td>
					</tr>
					</table>
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
