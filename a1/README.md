> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Glennetria Harold

### Assignment 1 Requirements:

*Four Parts:*

1. Distributied Version Control with Git and Bitbucket
2. Development Installations
3. Git command questions
4. Bitbucket repo Links:

    a) this assingment, and

    b) the completed tutorial (bitbucketstationlocations).

#### README.md file should include the following items:

* Screenshot of ampps installation running
* Screenshot of running JDK java Hello
* Screenshot of running Android Studio My First App using my name instead of “Hello World!”
* Git commands w/short descriptions
* Bitbucket repo links:
    - This assignment, and 
    - The completed tutorial repo above (bitbucketstationlocations)

#### Git commands w/short descriptions:

1. git init - start a new repository.
2. git status - list all the files that need to be committed.
3. git add - add a file to the staging area.
4. git commit - commit the file permanently in that version history.
5. git push -  sends committed changes made on a local directory to a remote repo.
6. git pull -  pull changes from a remote server and merge them to your local repo.
7. git config - set the author name/email address.

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/amppsScreenShot1.PNG)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/javaScreenShot.PNG)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/MyFirstApp.PNG)


#### Tutorial Links:
*Assignment 1:*
[A1 Repository Link](https://bitbucket.org/gjh16/lis4381 "gjh16 LIS4381 Main Repo")

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/gjh16/bitbucketstationlocations "Bitbucket Station Locations")