<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="This is my online protfolio created for LIS4381 with various mobile web app developments.">
		<meta name="author" content="Glennetria J. Harold">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body style="background-image: url('../img/bg.jpg'); background-repeat: no-repeat; background-size: cover;">

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Ampps installation, running JDK java Hello, and Screenshot of running Android Studio My First App using my name instead of “Hello World!” 
				</p>

				<h4>Java Installation</h4>
				<img src="img/javaScreenShot.PNG" class="img-responsive center-block" alt="JDK Installation">

				<h4>Android Studio Installation</h4>
				<img src="img/MyFirstApp.PNG" class="img-responsive center-block" alt="Android Studio Installation" width="500">

				<h4>AMPPS Installation</h4>
				<img src="img/amppsScreenShot1.PNG" class="img-responsive center-block" alt="AMPPS Installation">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
