<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Mark K. Jowett, Ph.D.">
		<link rel="icon" href="favicon.ico">

		<title>My Online Portfolio</title>

		<?php include_once("css/include_css.php"); ?>	

		<!-- Carousel styles -->
		<style type="text/css">
		 h2
		 {
			 margin: 0;     
			 color: #fff;
			 padding-top: 50px;
			 font-size: 52px;
			 font-family: "trebuchet ms", sans-serif;    
		 }
		 .item
		 {
			 background: #333;    
			 text-align: center;
			 height: 300px !important;
		 }
		 .carousel
		 {
			 margin: 20px 0px 20px 0px;
		 }
		 .bs-example
		 {
			 margin: 20px;
		 }
		</style>

	</head>
	<body style="background-image: url('img/bg.jpg'); background-repeat: no-repeat; background-size: cover;">

		<?php include_once("global/nav_global.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>

				<!-- Start Bootstrap Carousel  -->
				<div class="bs-example">
					<div
						id="myCarousel"
								class="carousel"
								data-interval="3000"
								data-pause="hover"
								data-wrap="true"
								data-keyboard="true"			
								data-ride="carousel">
						
    				<!-- Carousel indicators -->
						<ol class="carousel-indicators">
							<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
							<li data-target="#myCarousel" data-slide-to="1"></li>
							<li data-target="#myCarousel" data-slide-to="2"></li>
						</ol>   
						<!-- Carousel items -->
						<div class="carousel-inner">

							<!-- -Note: you will need to modify the code to make it work with *both* text and images.  -->
							
								<div class="active item" style="background: url(img/bg1.jpg) no-repeat left center; background-size: cover;">
									<a href="https://haroldglennetria.wixsite.com/glennetriah/blog" target="blank">
									<h2>Blog</h2>
									<div class="container">
										<div class="carousel-caption">
											<h3>View more info about activities she's involved in</h3>
											<p class="lead">from volunteering to development opportunities.</p>
										</div>
									</a></div>
								</div>
							

							
								<div class="item" style="background: url(img/bg2.jpg) no-repeat left center; background-size: cover;">
									<a href="https://haroldglennetria.wixsite.com/glennetriah" target="blank">
									<h2>Personal Website</h2>
									<div class="carousel-caption">
										<h3>Check out her personal website to learn more about her</h3>
										<p class="lead">Powered by Wix.</p>								
									</div>
								</a></div>
							
							
							<div class="item" style="background: url(img/bg3.jpg) no-repeat left center; background-size: cover;">
								<a href="https://www.linkedin.com/in/glennetriah/" target="blank">
								<h2>LinkedIn</h2>
								<div class="carousel-caption">
									<h3>View her professional experience at a glance</h3>
									<p class="lead">Feel free to connect with her while you're there!</p>						
								</div>
							</a></div>
							

						</div>
						<!-- Carousel nav -->
						<a class="carousel-control left" href="#myCarousel" data-slide="prev">
							<span class="glyphicon glyphicon-chevron-left"></span>
						</a>
						<a class="carousel-control right" href="#myCarousel" data-slide="next">
							<span class="glyphicon glyphicon-chevron-right"></span>
						</a>
					</div>
				</div>
				<!-- End Bootstrap Carousel  -->
				
				<?php
				include_once "global/footer.php";
				?>

			</div> <!-- end starter-template -->
    </div> <!-- end container -->

		<?php include_once("js/include_js.php"); ?>	
	  
  </body>
</html>
