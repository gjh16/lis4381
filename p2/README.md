# LIS4381 - Mobile Web Application Development

## Glennetria Harold

### Project 2 Requirements:

*Four Parts:*

1. Basic Server-Side validation
2. Add edit function
3. Add delete function
4. Create RSS feed file


#### README.md file should include the following items:

* Project 2 petstore tables
* Edit petstore table entry
* Edit petstore table error
* Home page with carousel images
* RSS Feed
* Bitbucket repo link for this assignments

#### Assignment Screenshots:

**Index.php**

![Main Page](img/p2.PNG)




|Edit petstore table entry (edit_petstore.php) | Edit petstore table error (edit_petstore_process.php) |
| ------- | ------- |
| ![edit_petstore.php](img/edit_petstore.PNG) | ![edit_petstore_process.php](img/edit_error.PNG) |

**Home Page Carousel**

![carousel](img/home.gif)



**RSS Feed**

![RSS Feed](img/rss.PNG)





#### Tutorial Links:

*Project 2:*
[P2 Repository Link](https://bitbucket.org/gjh16/lis4381 "gjh16 LIS4381 Main Repo")