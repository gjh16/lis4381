import java.util.Scanner;

public class Methods{
    public static void getRequirements(){
        System.out.println("\nDeveloper: Glennetria J. Harold");
        System.out.println("Temperature Conversion Program\n");
        System.out.println("Program converts user-entered temperatures into Fahrenheit or Celsius scales.");
        System.out.println("Note: upper or lower case letters permitted. Though, incorrect entries are not permitted");
        System.out.println("Note: Program does not validate numeric data (optional requirement).");
    }

    public static void convertTemp(){
        Scanner sc = new Scanner(System.in);
        double temp = 0.0;
        char choice;
        char type;

        do{
            System.out.print("\nFahrenheit to Celsius? Type \"f\", or Celsius to Fahrenheit? Type \"c\": ");
            type = sc.next().charAt(0);
            type = Character.toLowerCase(type);
            if(type == 'f'){
                System.out.print("Enter temperature in Fahrenheit: ");
                temp = sc.nextDouble();
                temp = ((temp - 32)*5)/9;
                System.out.println("Temperature in Celsius = " + temp);
            }
            else if(type == 'c'){
                System.out.print("Enter temperature in Celsius: ");
                temp = sc.nextDouble();
                temp = (temp * 9/5) + 32;
                System.out.println("Temperature in Fahrenheit = " + temp);
            }
            else{
                System.out.println("Incorrect entry. Please try again.");
            }
            System.out.print("\nDo you want to convert a temperature (y or n)?");
            choice =  sc.next().charAt(0);
            choice = Character.toLowerCase(choice);
        }while(choice == 'y');

        System.out.println("\nThank you for using our Temperature Conversion Program!");
    }
}