import java.util.Scanner;
import java.util.Random; //class to produce random numbers
class Methods{
	public static void getRequirements(){
		System.out.println("\nDeveloper: Glennetria J. Harold");
		System.out.println("Program prompts user for first name and age, then prints results.");
		System.out.println("Create four methods from the following requiremtns:");
		System.out.println("1) getRequirements(): Void method displays program requirements.");
		System.out.println("getUserInput(): Void method prompts for user input,\n\tthen calls two methods: myVoidMethod() and myValueReturningMethod().");
		System.out.println("myVoidMethod():");
		System.out.println("\ta. Accepts two arguments: String and Int.");
		System.out.println("\tb. Prints user's first name and age.");
		System.out.println("4) myValueReturningMethod():");
		System.out.println("\ta. Accepts two arguments: String and Int.");
		System.out.println("\tb. Prints user's first name and age.\n");
	}
	
	
	public static void getUserInput(){
		String myStr;
		String fname;
		int userAge;
		Scanner sc = new Scanner(System.in);

		System.out.print("Enter first name: ");
		fname = sc.next();
		System.out.print("Enter age: ");
		userAge = sc.nextInt();

		System.out.println();

		System.out.print("void method call: ");
		myVoidMethod(fname, userAge);

		System.out.print("value-returning method call: ");
		myStr = myValuerReturningMethod(fname, userAge);
		System.out.println(myStr);
	}

	public static void myVoidMethod(String first, int age){
		System.out.println(first + " is " + age);
	}

	public static String myValuerReturningMethod(String	first, int age){
		return first + " is " + age; // Because this method request a string it must return a string and concatnation helps convert int to string
	}
}