import java.util.Scanner;

class Main{
	public static void main(String args[]){
	//call static void methods i.e., no objects, non-value returning)
	Methods.getRequirements();

	//Java style String[] myArray
	//C++ style String myArray[]
	//call createArray() method in Methods class
	//return initialized array, array size determined by user

	int[] userArray = Methods.createArray(); // Java style array

	//calls generatePseudoRandomNumber() method, passing returned array above
	//Prints numbers determined by number user input
	Methods.generatePseudoRandomNumber(userArray);
	}
}