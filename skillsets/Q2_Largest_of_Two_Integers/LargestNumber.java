import java.util.Scanner;
public	class LargestNumber{
	private	static Scanner scn;
	public static void main(String[] args)
	{
		scn = new Scanner(System.in);
		int num1, num2;
		System.out.println("Developer: Glennetria J. Harold");
		System.out.println("Program evaluates largest of two integers.");
		System.out.println("Program does *not* check for non-numeric characters or non-integer values." + "\n");
		System.out.print("Enter first integer: ");
		num1 = scn.nextInt();
		System.out.print("Enter the second integer: ");
		num2 = scn.nextInt();

		if(num1 > num2){
			System.out.println("\n" + num1 + " is larger than " + num2);
		}

		else if(num1 < num2){
			System.out.println("\n" + num2 + " is larger than " + num1);
		}
		else if(num1 == num2){
			System.out.println("\n" + "Integers are equal.");
		}

	}
}