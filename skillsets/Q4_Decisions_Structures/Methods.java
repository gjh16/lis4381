import java.util.Scanner;
public class Methods
{
	//Creates metho without returning any value
	public static void getRequirements()
	{

		System.out.println("\nDeveloper: Glennetria J. Harold");
		System.out.println("Program evaluatees user-entered characters.");
		System.out.println("Use following characters: W or w, C or c, H or h, N or n.");
		System.out.println("User following decision structures: if...else, and switch.\n");
	}

	public static void getUserPhoneType()
	{
		Scanner sc = new Scanner(System.in);
		/*Note; Currently, there are no API methods to get a character from the scanner.
		Solution: get String using scanner.next() and invoke String.charAt(0) method on returned String*/

		System.out.println("Phone types: W or w (work), C or c (cell), H or h (home), N or n (none).");
		System.out.print("Enter phone type: ");

		String myStr = sc.next().toLowerCase();
		char myChar = myStr.charAt(0);

		System.out.println("\nif...else:");
		if(myChar == 'w'){
			System.out.println("Phone type: work");
		}
		else if(myChar == 'c'){
			System.out.println("Phone type: cell");
		}
		else if(myChar == 'h'){
			System.out.println("Phone type: home");
		}
		else if (myChar == 'n'){
			System.out.println("Phone type: none");
		}
		else{
			System.out.println("Incorrect character entry.");
		}
		System.out.println("\nswitch:");
		switch(myChar){
			case 'w':
				System.out.println("Phone type: work");
				break;
			case 'c':
				System.out.println("Phone type: cell");
				break;
			case 'h':
				System.out.println("Phone type: home");
				break;
			case 'n':
				System.out.println("Phone type: none");
				break;
			default:
				System.out.println("Incorrect character entry.");
				break;
		}
	}
}