import java.util.Scanner;
public class ArraysAndLoops{
	public static void main(String args[])
	{
		System.out.println("Developer: Glennetria J. Harold");
		System.out.println("Program loops through array of strings.");
		System.out.println("Use following values: dog, cat, bird, fish, insect.");
		System.out.println("Use following loop structures: for, enhanced for, while, do...while." + "\n");
		System.out.println("Note: Pretest loops: for, enhanced for, while. Posttest loop: do...while." + "\n");

		//set up array
		String[] animals = {"dog", "cat", "bird", "fish", "insect"};

		//for loop to print array
		System.out.println("for loop:");
		for (int i=0; i<animals.length; i++){
			System.out.println(animals[i]);
		}
		System.out.println();

		//enhanced for loop to print array
		System.out.println("Enhanced for loop:");
		for (String x : animals){
			System.out.println(x);
		}
		System.out.println();

		//while loop to print array
		System.out.println("while loop:");
		int j = 0;
		while (j < animals.length) {
			System.out.println(animals[j]);
			j++;
		}
		System.out.println();

		//do...while loop to print array
		System.out.println("do...while loop:");
		int k = 0;
		do{
			System.out.println(animals[k]);
			k++;
		}while(k < animals.length);
		System.out.println();
	}
}