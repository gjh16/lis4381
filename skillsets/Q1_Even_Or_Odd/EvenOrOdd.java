import java.util.Scanner;
public class EvenOrOdd{
    public static void main(String[] args){
     
     int i;
     Scanner j = new Scanner(System.in);
     System.out.println("Developer: Glennetria J. Harold");
     System.out.println("Program evaluates integers as even or odd.");
     System.out.println("Note: Program does *not* check for non-numeric characters." + "\n");
     System.out.print("Enter integer: ");
     i = j.nextInt();
     if(i % 2 == 0){
         System.out.println(i + " is an even number." + "\n");
        }
     else{
         System.out.println(i + " is an odd number." + "\n");
        }
    }
}