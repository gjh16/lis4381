import java.util.Scanner;
import java.util.Random; //class to produce random numbers
class Methods{
	public static void getRequirements(){
		System.out.println("\nDeveloper: Glennetria J. Harold");
		System.out.println("Print minimum and maximum iteger values.");
		System.out.println("Program prompts user to enter desired number of psuedorandom-generated integers (min 1).");
		System.out.println("Program validates user input for integers greater than 0.");
		System.out.println("Use following loop structures: for, enhanced for, while, do...while.\n");

		//Print min/max values
		System.out.println("Integer.MIN_VALUE = " + Integer.MIN_VALUE);
		System.out.println("Integer.MAX_VALUE = " + Integer.MAX_VALUE + "\n");

		System.out.println();
	}
	//Value-returning method (static requires no object)	
	public static int[] createArray(){
		Scanner sc = new Scanner(System.in);
		int arraySize = 0;

		//prompt user for number of randomly generated numbers
		System.out.print("Enter desired number of psuedorandom-generated integers (min 1): ");
		

		while (!sc.hasNextInt()){
			System.out.println("Not valid integer!\n");
			sc.next();
			System.out.print("Please try again. Enter valid integer (min 1): ");
		}
		arraySize = sc.nextInt();

		while(arraySize < 1){
			System.out.print("\nNumber must be greater than 0. Please enter integer greater than 0: ");
			while(!sc.hasNextInt()){
				System.out.print("\nNumber must be integer: ");
				sc.next();
				System.out.print("Pease try again. Enter an integer value greater than 0: ");
			}
			arraySize = sc.nextInt();
		}
		
		//Java style String[] myArray
		//C++ style String myArray[]
		int yourArray[] = new int[arraySize];
		return yourArray;

	}

	//nonvalue-returning method accepts in array arg (static requires no object)
	public	static void generatePseudoRandomNumber(int[] myArray){
		Random r = new Random();
		int i = 0;
		System.out.println("for loop:");
		for (i=0; i < myArray.length; i++){
			//nextInt(): get next random integer value from random number generator's sequence
			//nextInt(int n): pseudorandom, uniformity distributed int valuebetween 0 (incusive), and specified value (exclusive)
			//examples:

			//nextInt(upperbound) generated random numbers in rande 0 to upperbound minus 1
			//generate random values from 0-9, that is excluding 10
			//int uppperbound = 10;
			//int int_random = rand.nextInt(upperbound);

			//generate number from min to max (including both):
			//int x = r.nextInt(max - min +1) +min
			//int x = r.nextInt(10) + 1; //generate number bretween 1 and 10 inclusively


			//generate random integer within Integer.MIN_VALUE and Integer.MAX_VALUE
			System.out.println(r.nextInt());
			//System.out.println(r.nectInt(10)+1) //print numers between 1 and 10 inclusive
		}

		System.out.println("\nEnhanced for loop:");
		for(int n: myArray)
		{
			System.out.println(r.nextInt());
		}
		i = 0; //reset i to 0
		System.out.println("\nwhile loop:");
		while(i < myArray.length){
			System.out.println(r.nextInt());
			i++;
		}

		i = 0; //reset i to 0
		System.out.println("\ndo...while loop:");
		do{
			System.out.println(r.nextInt());
			i++;
		}while(i<myArray.length);

	}
}