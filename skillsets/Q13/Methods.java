import java.util.Scanner;
import java.text.DecimalFormat;

public class Methods{
    public static void getRequirements(){
        System.out.println("Developer: Glennetria J. Harold");
        System.out.println("Sphere Volume Program\n");
        System.out.println("Program calculates sphere volume in liquid U.S. gallons from user-entered diameter value in inces, ");
        System.out.println("and rounds to two decimal places.");
        System.out.println("Must use Java's *built-in* PI and pow() capabilities.");
        System.out.println("Program checks for non-integers and non-numeric values.");
        System.out.println("Program continues to prompt for user entry until no longer requested, prompt accepts upper and lower case letters.\n");
    }

    public static void validateUserInput(){
        Scanner sc = new Scanner(System.in);
        int diameter = 0;
        char choice = ' ';

        do{
            System.out.print("Please enter diameter in inches: ");

            while(!sc.hasNextInt()){
                System.out.println("Not valid integer!\n");
                sc.next();
                System.out.print("Please try again. Enter diameter in inches: ");
            }// end while

            diameter = sc.nextInt();

            System.out.println("\nSphere volume: " + getVolume(diameter) + " liquid U.S. gallons\n");

            System.out.print("Do you want to calculate another sphere volume (y or n)? ");
            choice = sc.next().charAt(0);
        }while(choice == 'y' || choice == 'Y');

        System.out.println("\nThank you for using out Sphere Volume Calculator!");
    
    }

    public static String getVolume(int diameter){
        double radius = (diameter/2.0);

        double volume = ((4.0/3) * Math.PI * Math.pow(radius, 3)/231);

        DecimalFormat f = new DecimalFormat("##.00");

        return String.format("%.2f", volume);
    }
}