# LIS4381 - Mobile Web Application Development

## Glennetria Harold

### Project 1 Requirements:

*Two Parts:*

1. My Business Car Application
2. Skillsets


#### README.md file should include the following items:

* Screenshot of running Android Studio first and second user interface
* Screenshot of all 3 Java skill sets running (7-9)


#### Assignment Screenshots:
*Screenshot of Android Studio - My Business Card*

| Activity_Main: | Activity_Info: |
| ------- | ------- |
| ![First User Interface Screenshot](img/gHarold_UI1.PNG) | ![Second User Interface Screenshot](img/gHarold_UI2.PNG) |


*Screenshots of Java Skill Sets*

|Skill Set 7: Random Array Using Methods and Data Validation | Skill Set 8: Largest Three Numbers | Skill Set 9: Array Runtime Data Validation |
| ------- | ------- | ------ |
| ![Skill Set 7 Screenshot](img/SS7.PNG) | ![Skill Set 8 Screenshot](img/SS8.PNG) | ![Skill Set 9 Screenshot](img/SS9.PNG) |


#### Assignment Links:

*Main Repo:* 
[P1 Repository Link](https://bitbucket.org/gjh16/lis4381 "gjh16 LIS4381 Main Repo")