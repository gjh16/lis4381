<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="This is my online protfolio created for LIS4381 with various mobile web app developments.">
		<meta name="author" content="Glennetria J. Harold">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body style="background-image: url('../img/bg.jpg'); background-repeat: no-repeat; background-size: cover;">

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<div class="text-justify">
					<p>
						<strong>Requirement:</strong> Backward-Engineer the screenshotsbelow, using <strong>your</strong> photo, contact information, and interests.
					</p>
					<p>Research how to do the following requirements(see screenshots below):
					<ol>
						<li>Create a launcher icon image and display it in both activities (screens)
						<li>Must add background color(s) to both activities
						<li>Must add border around image and button
						<li>Must add text shadow(button)
					</ol>
					</p>
				</div>
				<table style="width:100%;">
					<tr>
						<th style="border-right: 5px solid white; padding:0 0 10px 70px;">User Interface 1</th>
						<th style= "padding:0 0 10px 70px;">User Interface 2</th>
					</tr>
					<tr>
						<td style="border-right: 5px solid white;"><img src="img/gHarold_UI1.PNG" class="img-responsive center-block" alt="UI 1"></td>
						<td><img src="img/gHarold_UI2.PNG" class="img-responsive center-block" alt="UI 2"></td>
					</tr>
					</table>
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
