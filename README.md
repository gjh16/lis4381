> **NOTE:** A README.md file should be placed at the **root of each of your repos directories**

# LIS4381 - Mobile Web Application Development

## Glennetria Harold

### LIS4381 Requirements

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Setup Distributed Version Control
    - Complete Bitbucket tutorials (BitbucketStationLocations)
    - Download and install AMPPS
    - Download and install JDK
    - Download and install Android Studio
    - Provide screenshots of installation
    - Provide git command descriptions
2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create Healthy Recipes Android app
    - Provide Screenshots of completed app
    - Provide Screenshots of completed Java skill sets
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create My Event Android App
    - Provide Screenshots of completed app
    - Provide Screenshots of ERD
    - Provide Links to SQL files
    - Provide Screenshots of completed Java skill sets
4. [A4 README.md](a4/README.md "My A4 README.md file")
    - My Online Portfolio Web App Main
    - Failed Validation
    - Passed Validation
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Screenshot of running pet stores table with information from server
    - Screenshot of server-side validation
    - 1 Java Skillset
    - 2 PHP Skillsets
6. [P1 README.md](p1/README.md "My p1 README.md file")
    - Backwards engineer business card application
    - Provide Screenshots of completed Java skill sets
7. [P2 README.md](p2/README.md "My p2 README.md file")
    - Basic Server-Side validation
    - Add edit function
    - Add delete function
    - Create RSS feed file