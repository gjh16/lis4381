-- MySQL Script generated by MySQL Workbench
-- Mon Oct  5 01:11:57 2020
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema gjh16
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `gjh16` ;

-- -----------------------------------------------------
-- Schema gjh16
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `gjh16` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ;
SHOW WARNINGS;
USE `gjh16` ;

-- -----------------------------------------------------
-- Table `gjh16`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gjh16`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gjh16`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_zip` INT UNSIGNED NOT NULL,
  `pst_phone` BIGINT UNSIGNED NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_ytd_sales` DECIMAL(10,2) NOT NULL,
  `pst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gjh16`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gjh16`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gjh16`.`customer` (
  `cus_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cus_fname` VARCHAR(15) NOT NULL,
  `cus_lname` VARCHAR(30) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cus_state` CHAR(2) NOT NULL,
  `cus_zip` INT UNSIGNED NOT NULL,
  `cus_phone` BIGINT UNSIGNED NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(8,2) NOT NULL,
  `cus_total_sales` DECIMAL(8,2) NOT NULL,
  `cus_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cus_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `gjh16`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `gjh16`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `gjh16`.`pet` (
  `pet_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_id` SMALLINT UNSIGNED NOT NULL,
  `cus_id` SMALLINT UNSIGNED NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m', 'f') NOT NULL,
  `pet_cost` DECIMAL(6,2) NOT NULL COMMENT 'what the pet cost for the petstore to buy',
  `pet_price` DECIMAL(6,2) NOT NULL COMMENT 'how much the pet is sold to cus for',
  `pet_age` INT NOT NULL COMMENT 'age is in weeks, divide by 52 for years',
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_sale_date` DATE NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_neuter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pet_id`),
  INDEX `fk_pet_petstore_idx` (`pst_id` ASC) VISIBLE,
  INDEX `fk_pet_customer1_idx` (`cus_id` ASC) VISIBLE,
  CONSTRAINT `fk_pet_petstore`
    FOREIGN KEY (`pst_id`)
    REFERENCES `gjh16`.`petstore` (`pst_id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_pet_customer1`
    FOREIGN KEY (`cus_id`)
    REFERENCES `gjh16`.`customer` (`cus_id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `gjh16`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `gjh16`;
INSERT INTO `gjh16`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_state`, `pst_city`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Claws and Paws', '123 Main St.', 'CA', 'Los Angeles', 90007, 3239827171, 'support@clawsandpaws.com', 'https://www.clawsandpaws.com', 50000, 'not very clean');
INSERT INTO `gjh16`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_state`, `pst_city`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Furry Friends', '1435 Chase Dr.', 'IL', 'Chicago', 67399, 8392223829, 'admin@furryfriends.com', 'https://www.furryfriends.com', 65000, NULL);
INSERT INTO `gjh16`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_state`, `pst_city`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Hotel for Dogs', '8827 N Main St.', 'FL', 'Jacksonville', 32208, 9046844447, 'staff@hotelfordogs.com', 'https://www.hotelfordogs.com', 87000, NULL);
INSERT INTO `gjh16`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_state`, `pst_city`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Pets R Friends', '220 Ocean Drive', 'FL', 'Tampa', 32022, 8029338298, 'notfood@petsrfriends.org', 'https://www.petsrfriends.org', 32200, NULL);
INSERT INTO `gjh16`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_state`, `pst_city`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'A Pet\'s Purpose', '39233 A St.', 'GA', 'Atlanta', 34201, 3502938475, 'staff@apetspurpose.org', 'https://www.apetspurpose.org', 49000, NULL);
INSERT INTO `gjh16`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_state`, `pst_city`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Purrs and Grrs', '751 San Luis Dr.', 'FL', 'Tallahassee', 32304, 8506693394, 'staff@purrsngrrs.com', 'https://www.purrsngrrs.com', 9000, NULL);
INSERT INTO `gjh16`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_state`, `pst_city`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'The Pet Palace', '800 W Tennesee St.', 'FL', 'Tallahassee', 32304, 8506649283, 'petpalace@gmail.com', 'https://www.petpalace.com', 22000, NULL);
INSERT INTO `gjh16`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_state`, `pst_city`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'A Pet Place NY', '219 E 14th St.', 'NY', 'New York', 10025, 2122033394, 'apetplaceny@gmail.com', 'https://www.apetplace.com', 102200, NULL);
INSERT INTO `gjh16`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_state`, `pst_city`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Adopt Not Shop', '11233 Safe Pl.', 'AK', 'Kelowana', 99502, 2505548999, 'contact@adoptnotshop.net', 'https://www.adoptnotshop.net', 65000, NULL);
INSERT INTO `gjh16`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_state`, `pst_city`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Cuddly Critters', '2028 North Ave.', 'TX', 'Houston', 77598, 2819920003, 'contact@cuddlycritters.com', 'https://www.cuddlycritters.com', 62300, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `gjh16`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `gjh16`;
INSERT INTO `gjh16`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Billy', 'Bob', '504 N Main St.', 'Los Angeles', 'CA', 90007, 3233196060, 'billybob04@aol.com', 139.52, 987.61, 'note1');
INSERT INTO `gjh16`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Sally', 'Sue', '18392 Rutherford Cir.', 'Charleston', 'NC', 21192, 5043339483, 'sallys9281@hotmail.com', 49.92, 1356.23, NULL);
INSERT INTO `gjh16`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Curious', 'George', '192 Monkey Dr.', 'Houston', 'TX', 66182, 2038283746, 'cruisorgeorge93@gmail.com', 0.0, 182.09, NULL);
INSERT INTO `gjh16`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Beyonce', 'Knowles', '112 Boss Ln.', 'San Diego', 'CA', 90210, 9002837228, 'beyhive@beyonce.com', 0.0, 73744.29, 'It\'s BEYONCE');
INSERT INTO `gjh16`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Joe', 'Jonas', '9223 Mio Rd.', 'Ney York', 'NY', 11209, 9139920018, 'joe@jonas.com', 93.20, 4922.02, NULL);
INSERT INTO `gjh16`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Sasuke', 'Uchiha', '93 Hiddin Leaf Vil.', 'Pheonix', 'AZ', 22516, 7855547856, 'SASUKEEEEE@naruto.com', 20.22, 3239.34, 'SASUKEEEEEEEE');
INSERT INTO `gjh16`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Naruto ', 'Uzumaki', '500 Believe It Ln.', 'Pheonix', 'AZ', 22517, 7850029384, 'hokage@leafvillege.com', 948.98, 2393.76, 'NARUTOOOOOO');
INSERT INTO `gjh16`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Soma', 'Yukihira', '101 Shokugeki Cir.', 'Miami', 'FL', 32700, 4502839929, 'icancookbetterthanyou@comcast.com', 132.99, 289.92, 'the best anime. CHANGE MY MIND');
INSERT INTO `gjh16`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Salt', 'Bae', '69 Meme Ln.', 'Jacksonville', 'FL', 32219, 9045583028, 'hoohaha@outlook.com', 12.89, 203.10, NULL);
INSERT INTO `gjh16`.`customer` (`cus_id`, `cus_fname`, `cus_lname`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Barak', 'Obama', '890 Presidential Dr.', 'Kelowana', 'AK', 99502, 2505436533, 'barakobama@outlook.com', 40.39, 40.39, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `gjh16`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `gjh16`;
INSERT INTO `gjh16`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 3, 5, 'Doberman', 'm', 325, 525, 52, 'black/tan', '2009-07-05', 'y', 'y', NULL);
INSERT INTO `gjh16`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 2, NULL, 'Chiwawa', 'm', 85, 165, 78, 'white/brown', NULL, 'n', 'n', NULL);
INSERT INTO `gjh16`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 1, 4, 'American Bobtail', 'f', 275, 385, 104, 'black', '2011-12-24', 'y', 'y', 'she bites');
INSERT INTO `gjh16`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 1, 4, 'American Longhair', 'm', 300, 275, 156, 'white', '2005-08-01', 'n', 'y', NULL);
INSERT INTO `gjh16`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 5, NULL, 'Pitbull', 'm', 130, 145, 28, 'grey', NULL, 'y', 'n', NULL);
INSERT INTO `gjh16`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 2, 3, 'American Terrior', 'f', 95, 167, 2, 'grey', '2010-11-12', 'n', 'n', NULL);
INSERT INTO `gjh16`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 7, 10, 'American Longhair', 'f', 48, 38, 250, 'black/white', '2007-09-19', 'n', 'n', NULL);
INSERT INTO `gjh16`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 8, NULL, 'Ragdoll', 'f', 130, 166, 8, 'light brown', NULL, 'n', 'y', NULL);
INSERT INTO `gjh16`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 9, 2, 'American Bobtail', 'm', 30, 47, 80, 'dark brown', '2020-05-27', 'y', 'y', NULL);
INSERT INTO `gjh16`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 6, NULL, 'Boa Constrictor', 'f', 250, 300, 208, 'green/yellow', NULL, 'y', 'n', NULL);
INSERT INTO `gjh16`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 10, 7, 'Giant Toad', 'm', 850, 1008, 398, 'green', '2004-03-22', 'n', 'y', NULL);
INSERT INTO `gjh16`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 10, 6, 'Snake Orochimaru', 'm', 2525, 3028, 3974, 'purple', '2003-11-24', 'n', 'y', 'has an obsession with immortality');
INSERT INTO `gjh16`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 10, 7, 'Toad Sage', 'm', 1000, 1800, 9837, 'green/brown', '2005-09-04', 'n', 'n', 'needs it\'s staff');
INSERT INTO `gjh16`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 4, 9, 'German Shephard', 'f', 195, 178, 54, 'light brown/black', '2014-05-29', 'y', 'n', NULL);
INSERT INTO `gjh16`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 4, 8, 'Orpington Chick', 'm', 15, 275, 254, 'white', '2017-12-25', 'n', 'y', 'DO NOT EAT');

COMMIT;

