<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="This is my online protfolio created for LIS4381 with various mobile web app developments.">
		<meta name="author" content="Glennetria J. Harold">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment1</title>	
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body style="background-image: url('../img/bg.jpg'); background-repeat: no-repeat; background-size: cover;">

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<div class="text-justify">
					<p><strong>Database Requirements:</strong> Frequently, not only will you be asked to design and develop Web applications, 
					but you will also be asked to create (design) database solutions that interact with the Web application—and, in fact, the data repository is the *core* of all Web applications. 
					Hence, the following business requirements. 
					</p>
					<p>A pet store owner, who owns a number of pet stores, requests that you develop a Web application whereby he and his team can record, track, and maintain relevant company data, 
					based upon the following business rules:
					</p>
					<ol>
						<li>A customer can buy many pets, but each pet, if purchased, is purchased by only one customer.
						<li>A store has many pets, but each pet is sold by only one store.
					</ol>
					<p><strong>Remember:</strong> an organization’s business rules are the key to a well-designed database.</p>
					<p>For the Pet's R-Us business,it's important to ask the following questions to get a better idea of how the database and Web application should work together:
					<ul>
						<li>Can a customer exist without a pet? Seems reasonable. Yes. (optional)
						<li>Can a pet exist without a customer? Again, yes. (optional)
						<li>Can a pet store not have any pets? It wouldn’t be a pet store. (mandatory)
						<li>Can a pet exist without a pet store? Not in this design. (mandatory)
					</ul>
					</p>
					<strong>Solutions:</strong>
					<ul>
						<li><a href="docs/a3.mwb">A3 MWB</a>
						<li><a href="docs/a3.sql">A3 SQL</a>
					</ul>
				</div>

				<h4>Petstore ERD</h4>
				<img src="img/gjh16_a3_ERD.png" class="img-responsive center-block" alt="Petstore ERD">

				</br>
				</br>
				</br>

				<table style="width:100%;">
					<tr>
						<th style="border-right: 5px solid white;">User Interface 1</th>
						<th style="border-right: 5px solid white;">Animated Version</th>
						<th>User Interface 2</th>
					</tr>
					<tr>
						<td style="border-right: 5px solid white;"><img src="img/ui1.PNG" class="img-responsive center-block" alt="UI1"></td>
						<td style="border-right: 5px solid white;"><img src="img/concert.gif" class="img-responsive center-block" alt="UI GIF"></td>
						<td><img src="img/ui2.PNG" class="img-responsive center-block" alt="UI2"></td>
					</tr>
					</table>
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
