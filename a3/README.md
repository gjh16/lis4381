# LIS4381 - Mobile Web Application Development

## Glennetria Harold

### Assignment 3 Requirements:

*Three Parts:*

1. Petstore ERD
2. My Event Mobile App
2. Links to A3 mwb and sql files
3. Bitbucket repo link 


#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of running Android Studio first and second user interface
* Screenshot of all 3 Java skill sets running
* Link to the following files:
    - a3.mwb
    - s3.sql


#### Assignment Screenshots:
*Screenshot of ERD*
![Petstore ERD](img/gjh16_a3_ERD.png)

*Screenshot of Android Studio - My Event*

| First UI: | Animated | Second UI: |
| ------- | ------- | ------- |
| ![First User Interface Screenshot](img/ui1.PNG) | ![Interface Animated Gif](img/concert.gif) | ![Second User Interface Screenshot](img/ui2.PNG) |

*Screenshots of Java Skill Sets*

|Skill Set 4: Decision Structures | Skill Set 5: Random Array | Skill Set 6: Java Methods |
| ------- | ------- | ------ |
| ![Skill Set 4 Screenshot](img/SS4_Decision_Structures.PNG) | ![Skill Set 5 Screenshot](img/SS5_Random_Array.PNG) | ![Skill Set 6 Screenshot](img/SS6_Java_Methods.PNG) |


#### Assignment Links:
*a3.mwb File:* 
[a3.mwb](docs/a3.mwb "a3 mwb file")

*a3.sqp File:* 
[a3.sql](docs/a3.sql "a3 sql file")

*Main Repo:* 
[A3 Repository Link](https://bitbucket.org/gjh16/lis4381 "gjh16 LIS4381 Main Repo")