# LIS4381 - Mobile Web Application Development

## Glennetria Harold

### Assignment 5 Requirements:

*Four Parts:*

1. Basic Server-Side validation
2. 1 Java Skillset
3. 2 PHP Skillsets
3. Bitbucket repo link 


#### README.md file should include the following items:

* Screenshot of running pet stores table with information from server
* Screenshot of server-side validation
* Screenshot of all 3 skill sets running
* Bitbucket repo link for this assignments

#### Assignment Screenshots:

**Pet Stores Table**

![Main Page](img/A5_index.PNG)



**Server-Side Validation: **

![Serverside Validation](img/A5_error.PNG)

*Screenshots of Skillsets*

**Skill Set 13 Sphere Volume Program** 

![Skill Set 10 Screenshot](img/SS13.PNG)

|Skill Set 14 Simple Calculator (index.php) | Skill Set 14 Simple Calculator (process.php with exponetiation) |
| ------- | ------- |
| ![Skill Set 14 index.php](img/SS14_img1.PNG) | ![Skill Set 14 process.php](img/SS14_img2.PNG) |

|Skill Set 14 Simple Calculator (index.php divide by 0) | Skill Set 14 Simple Calculator (process.php divide by 0 error) |
| ------- | ------- |
| ![Skill Set 14 index.php](img/SS14_img3.PNG) | ![Skill Set 14 process.php](img/SS14_img4.PNG) |

|Skill Set 15 Write/Read File (index.php) | Skill Set 15 Write/Read File (process.php) |
| ------- | ------- |
| ![Skill Set 15 index.php](img/SS15_img1.PNG) | ![Skill Set 15 process.php](img/SS15_img2.PNG) |


#### Tutorial Links:
*Assignment  5:*
[A5 Repository Link](https://bitbucket.org/gjh16/lis4381 "gjh16 LIS4381 Main Repo")