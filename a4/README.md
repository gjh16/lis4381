# LIS4381 - Mobile Web Application Development

## Glennetria Harold

### Assignment 4 Requirements:

*Four Parts:*

1. My Online Portfolio Web App Main
2. Failed Validation
3. Passed Validation
3. Bitbucket repo link 


#### README.md file should include the following items:

* Screenshot of running Online Portfolio Web App
* Screenshot of all 3 Java skill sets running
* Bitbucket repo link for this assignment

#### Assignment Screenshots:
*Screenshot of Online Portfolio*
Landing Page:
![Main Page](img/main.png)
Failed Validation: 
![First User Interface Screenshot](img/failedvd.PNG)
Passed Validation:
![Second User Interface Screenshot](img/passedvd.PNG)
*Screenshots of Java Skill Sets*

|Skill Set 10: | Skill Set 11 | Skill Set 12: |
| ------- | ------- | ------ |
| ![Skill Set 10 Screenshot](img/SS10.PNG) | ![Skill Set 11 Screenshot](img/SS11.PNG)| ![Skill Set 12 Screenshot](img/SS12.PNG) |


#### Tutorial Links:
*Assignment  4:*
[A4 Repository Link](https://bitbucket.org/gjh16/lis4381 "gjh16 LIS4381 Main Repo")